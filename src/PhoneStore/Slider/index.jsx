import React, { Component } from 'react';
// import Slide1 from '.../assets/slide_1.jpg';

class Slider extends Component {
    render() {
        return (

            <div id="demo" className="carousel slide" data-ride="carousel">
                {/* Indicators */}
                <ul className="carousel-indicators">
                    <li data-target="#demo" data-slide-to={0} className="active" />
                    <li data-target="#demo" data-slide-to={1} />
                    <li data-target="#demo" data-slide-to={2} />
                </ul>
                {/* The slideshow */}
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img src="./assets/slide_1.jpg" width="100%" height="500" alt="Slide1" />
                    </div>
                    <div className="carousel-item">
                        <img src="./assets/slide_2.jpg" width="100%" height="500" alt="Slide2" />
                    </div>
                    <div className="carousel-item">
                        <img src="./assets/slide_3.jpg" width="100%" height="500" alt="Slide3" />
                    </div>
                </div>
                {/* Left and right controls */}
                <a className="carousel-control-prev" href="#demo" data-slide="prev">
                    <span className="carousel-control-prev-icon" />
                </a>
                <a className="carousel-control-next" href="#demo" data-slide="next">
                    <span className="carousel-control-next-icon" />
                </a>
            </div>

        );
    }
}

export default Slider;
import React, { Component } from 'react'

class Header extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                    {/* Brand/logo */}
                    <a className="navbar-brand col-9 ml-0" href="#">CyberSoft</a>
                    {/* Links */}
                    <ul className="navbar-nav col-3  mr-0">
                        <li className="nav-item">
                            <a className="nav-link" href="#">Home</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Menu</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Products</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Forum</a>
                        </li>
                    </ul>
                </nav>

            </div>


        );
    }
}

export default Header;
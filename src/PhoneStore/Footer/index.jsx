import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <section id="promotion" className="container-fluid pt-5 pb-5 bg-dark mt-3 mb-3">
                <h1 className="text=center text-white text-center">PROMOTION</h1>
                <div className="container bg-light pt-5 pb-5">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-4">
                            <div className="container">
                                <img src="./assets/promotion_1.png" alt="" style={{ maxWidth: '100%' }} />

                            </div>

                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4">
                            <div className="container">
                                <img src="./assets/promotion_2.png" alt="" style={{ maxWidth: '100%' }} />

                            </div>

                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-4">
                            <div className="container">
                                <img src="./assets/promotion_3.jpg" alt="" style={{ maxWidth: '100%' }} />

                            </div>

                        </div>

                    </div>

                </div>
            </section>
        )
    }
}

export default Footer;
import React, { Component } from 'react'

class Product extends Component {
    render() {

        const { img, name, desc } = this.props.item;
        return (
            <div className="card bg-light" style={{ width: 220 }}>
                <img className="card-img-top" src={img} alt="" style={{ maxWidth: '100%', height: 250 }} />
                <div className="card-body text-center">
                    <h4 className="card-title text-center">{name}</h4>
                    <p className="card-text">{desc}</p>
                    <a href="#" className="btn btn-primary mr-1">Detail</a>
                    <a href="#" className="btn btn-danger">Cart</a>
                </div>
            </div>

        )
    }
}

export default Product;